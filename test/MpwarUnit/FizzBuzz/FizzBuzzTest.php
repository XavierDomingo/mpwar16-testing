<?php

namespace MpwarUnit\FizzBuzz;

use Mpwar\FizzBuzz\FizzBuzz;
use Mpwar\FizzBuzz\Scenario;

use PHPUnit_Framework_TestCase;

final class FizzBuzzTest extends PHPUnit_Framework_TestCase
{
    const NUMBER = 24;

    /**
     * @test
     */
    public function shouldWorkWithNotCases()
    {
        $cases = [];
        $fizzBuzz = new FizzBuzz($cases);

        $this->assertSame((string)self::NUMBER, $fizzBuzz->calculateResult(self::NUMBER));
    }

    /**
     * @test
     */
    public function shouldWorkWithASingleCase()
    {
        $firstCase = $this->getMock(Scenario::class);

        $cases = [$firstCase];
        $fizzBuzz = new FizzBuzz($cases);

        $firstCase
            ->method("isSatisfied")
            ->will($this->returnValue(true));

        $firstCase
            ->method("resultValue")
            ->will($this->returnValue('first'));

        $this->assertSame('first', $fizzBuzz->calculateResult(self::NUMBER));
    }

    /**
     * @test
     */
    public function shouldWorkWithThreeCase()
    {
        $firstCase = $this->getMock(Scenario::class);
        $secondCase = $this->getMock(Scenario::class);
        $thirdCase = $this->getMock(Scenario::class);

        $cases = [$firstCase, $secondCase, $thirdCase];
        $fizzBuzz = new FizzBuzz($cases);

        $firstCase
            ->method("isSatisfied")
            ->will($this->returnValue(true));

        $firstCase
            ->method("resultValue")
            ->will($this->returnValue('first'));

        $secondCase
            ->method("isSatisfied")
            ->will($this->returnValue(false));

        $thirdCase
            ->method("isSatisfied")
            ->will($this->returnValue(true));

        $thirdCase
            ->method("resultValue")
            ->will($this->returnValue('third'));


        $this->assertSame('first third', $fizzBuzz->calculateResult(self::NUMBER));
    }

    /**
     * @test
     */
    public function shouldWorkWithDefaultCase()
    {
        $firstCase = $this->getMock(Scenario::class);
        $normalCase = $this->getMock(Scenario::class);

        $cases = [$firstCase];
        $fizzBuzz = new FizzBuzz($cases, $normalCase);

        $firstCase
            ->method("isSatisfied")
            ->will($this->returnValue(false));

        $this->assertSame((string)self::NUMBER, $fizzBuzz->calculateResult(self::NUMBER));
    }
}
<?php

namespace Mpwar\Doubles;

interface PrimeNumberService
{
    public function isPrime($number);
}
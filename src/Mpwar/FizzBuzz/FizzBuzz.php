<?php

namespace Mpwar\FizzBuzz;

class FizzBuzz
{
    private $scenarios;

    public function __construct(array $allScenarios)
    {
        $this->scenarios = $allScenarios;
    }

    public function calculateResult($inputValue)
    {
        $stackResult = [];
        foreach ($this->scenarios as $scenario) {
            if ($scenario->isSatisfied($inputValue)) {
                $stackResult[] = $scenario->resultValue();
            }
        }

        if (empty($stackResult)) return (string)$inputValue;
        return implode(' ', $stackResult);
    }
}
<?php

namespace Mpwar\FizzBuzz;

interface Scenario
{
    public function resultValue();
    public function isSatisfied($number);
}